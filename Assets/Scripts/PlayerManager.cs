﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    ///Animator anim;
	// Use this for initialization
    public float speedX;
    public float jumpSpeedY;
    public float delayBeforeDoubleJump;
    bool facingRight, Jumping, isGrounded, canDoubleJump;
    bool isHurting, isDead = false;
    int healthPoints = 3;
    public GameObject leftBullet, rightBullet;
    float speed;
    Transform firePos;
    public Camera camera;
    private Vector3 offset;
    public AudioClip[] audiocl;


    //State
    public int currentHealth = 0;
    public int maxHealth = 5;

    //References
    private gameMaster gm;

    Animator anim;
    Rigidbody2D rb;
	void Start () {
        //anim = GetComponent<Animator>();
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        facingRight = true;
        firePos = transform.FindChild("firePos");
        currentHealth = maxHealth;

        offset = camera.transform.position - transform.position;

        gm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<gameMaster>();
        if(gm != null){
            print("it works");
        }
	}
	
	// Update is called once per frame
	void Update () {

        camera.transform.position = transform.position + offset;
        MovePlayer(speed);

        if(Input.GetKeyDown(KeyCode.LeftArrow)){
            speed = -speedX;
        }

        if(Input.GetKeyUp(KeyCode.LeftArrow)){
            speed = 0;
        }


        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            speed = +speedX;
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            speed = 0;
        }

        if(Input.GetKeyDown(KeyCode.UpArrow) && isGrounded){
            Jump();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && canDoubleJump)
        {
            Jump();
        }

        Flip();

        if(Input.GetKeyDown(KeyCode.Space)){
            Fire();
        }
        /*if (Input.GetKeyDown(KeyCode.W))
        {
            anim.SetInteger("State", 1);
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            anim.SetInteger("State", 0);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            anim.SetInteger("State", 2);
        }
        if (Input.GetKeyUp(KeyCode.R))
        {
            anim.SetInteger("State", 0);
        }*/

        if(currentHealth > maxHealth){
            currentHealth = maxHealth;
        }

        if(currentHealth <= 0){
            Die();
        }
    }

    void MovePlayer(float playerSpeed){

        if(playerSpeed < 0 && !Jumping || playerSpeed > 0 && !Jumping){
            anim.SetInteger("State", 2);
        }
        if(playerSpeed == 0 && !Jumping){
            anim.SetInteger("State", 0);
        }

        rb.velocity = new Vector3(speed, rb.velocity.y, 0);
    }

    void Flip(){
        if(speed > 0 && !facingRight || speed < 0 && facingRight){
            facingRight = !facingRight;

            Vector3 temp = transform.localScale;
            temp.x *= -1;
            transform.localScale = temp;
        }
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name.Equals("Fire")){
            healthPoints -= 1;
            print("healhPoints" + healthPoints);
            print("Herido");
        }

        if(collision.gameObject.name.Equals("Fire") && healthPoints > 0){

            anim.SetTrigger("isHurting");
            //StartCoroutine("Hurt");
        } else {
            print("Esta muerto");
            speed = 0;
            this.isDead = true;
            anim.SetTrigger("isDead");
            Destroy(gameObject, 3f);
        }
    }*/

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "GROUND"){
            isGrounded = true;
            canDoubleJump = false;
            Jumping = false;
            anim.SetInteger("State", 0);
        }

    }

    public void Jump(){
        if (isGrounded)
        {
            Jumping = true;
            isGrounded = false;
            rb.AddForce(new Vector2(rb.velocity.x, jumpSpeedY));
            anim.SetInteger("State", 3);
            Invoke("EnableDoubleJump", delayBeforeDoubleJump);
        }
        if (canDoubleJump)
        {
            canDoubleJump = false;
            rb.AddForce(new Vector2(rb.velocity.x, jumpSpeedY));
            anim.SetInteger("State", 3);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Coin")){
            Destroy(collision.gameObject);
            gm.points += 1;
        }
    }

    void Fire(){
        if (facingRight)
            Instantiate(rightBullet, firePos.position, Quaternion.identity);
        if (!facingRight)
            Instantiate(leftBullet, firePos.position, Quaternion.identity);
    }

    void EnableDoubleJump() {
        canDoubleJump = true;
    }


    void Die(){

        //Restart
        Application.LoadLevel(Application.loadedLevel);
    }

    public void Damage(int dmg){
        if(currentHealth >= 3){
            currentHealth -= dmg;
        } else {
            dmg = 2;
            currentHealth -= dmg;
        }

    }

}
