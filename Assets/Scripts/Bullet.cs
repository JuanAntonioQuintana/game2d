﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.isTrigger != true){
            print("jugador tocado");
            if(collision.CompareTag("Player")){
                collision.GetComponent<PlayerManager>().Damage(1);
            }

            Destroy(gameObject);
        }
    }
}
