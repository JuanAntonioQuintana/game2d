﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightManager : MonoBehaviour {

    public float speedX;
    bool facingRight;
    float speed;
    Animator anim;
    Rigidbody2D rb;
    public int currentHealth;
    public int maxHealth = 100;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        facingRight = true;
        currentHealth = maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
        MovePlayer(speed);

        if(Input.GetKeyDown(KeyCode.A)){
            speed = -speedX;
        }

        if(Input.GetKeyUp(KeyCode.A)){
            speed = 0;
        }

        if(Input.GetKeyDown(KeyCode.D)){
            speed = +speedX;
        }

        if(Input.GetKeyUp(KeyCode.D)){
            speed = 0;
        }

        if(Input.GetKeyDown(KeyCode.W)){
            anim.SetInteger("Status", 2);
        }

        Flip();

        if(currentHealth > maxHealth){
            currentHealth = maxHealth;
        }
        if(currentHealth <= 0){
            Die();
        }

	}

    void MovePlayer(float playerSpeed){
        if(playerSpeed < 0 || playerSpeed > 0){
            anim.SetInteger("Status", 1);
        }
        if(playerSpeed == 0){
            anim.SetInteger("Status", 0);
        }
        rb.velocity = new Vector3(speed, rb.velocity.y, 0);
    }

    void Flip(){
        if (speed > 0 && !facingRight || speed < 0 && facingRight)
        {
            facingRight = !facingRight;

            Vector3 temp = transform.localScale;
            temp.x *= -1;
            transform.localScale = temp;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "GROUND"){
            anim.SetInteger("Status", 0);
        }
    }

    void Die(){
        Application.LoadLevel(Application.loadedLevel);
    }
}
