﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

    public Sprite[] HeartSprites;

    public Image HeartUI;

    public PlayerManager player;

    void start(){
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>();

    }

    void Update() {
        HeartUI.sprite = HeartSprites[player.currentHealth];
    }
}
