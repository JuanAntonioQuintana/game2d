﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCtrl : MonoBehaviour {


    public Vector2 speed;
    private float value = 5f;

    Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = speed;
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = speed;
        rb.transform.localScale = new Vector3(value*Time.deltaTime, value * Time.deltaTime, 0);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag("GROUND")){
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
