﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack_Cone : MonoBehaviour {

    public TurretAI turreAI;
    public bool isLeft = false;


    void Awake(){
        turreAI = gameObject.GetComponentInParent<TurretAI>();
       
    }

    void OnTriggerStay2D(Collider2D collision)
    {

        Debug.Log("hello world");
        if(collision.CompareTag("Player")){
            print("Entra aquí");
            if(isLeft){
                turreAI.Attack(false);
            } else {
                turreAI.Attack(true);
            }
        }
    }
}
