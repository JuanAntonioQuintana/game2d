﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text timer;
    public float t = 100.0f;

    public void Update(){
        /*print(t);*/
        this.t -= Time.deltaTime;
        if(this.t < 0){
            //t = 100.0f;
            Application.LoadLevel(Application.loadedLevel);
        }
        timer.text = t.ToString("f0");
    }
}
